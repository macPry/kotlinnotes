package com.macpry.kotlinnotes.data

import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.verify
import java.util.*

/* Copyright © Maciej Przybyl * All rights reserved. */

private val NOTE_ID = UUID.randomUUID().toString()
private val NOTE = Note(NOTE_ID)

class NotesRepositoryTest {

    private lateinit var notesRepository: NotesDataSource
    @Mock lateinit var notesLocalDataSource: NotesDataSource

    @Before
    fun setupRepository() {
        MockitoAnnotations.initMocks(this)
        notesRepository = NotesRepository(notesLocalDataSource)
    }

    @Test
    fun getNotesTest() {
        notesRepository.getNotes()
        verify(notesLocalDataSource).getNotes()
    }

    @Test
    fun getNoteTest() {
        notesRepository.getNote(NOTE_ID)
        verify(notesLocalDataSource).getNote(NOTE_ID)
    }

    @Test
    fun saveNoteTest() {
        notesRepository.saveNote(NOTE)
        verify(notesLocalDataSource).saveNote(NOTE)
    }

    @Test
    fun deleteNoteTest() {
        notesRepository.deleteNote(NOTE_ID)
        verify(notesLocalDataSource).deleteNote(NOTE_ID)
    }
}