package com.macpry.kotlinnotes.notes.ui

import android.view.ViewGroup
import android.widget.LinearLayout
import com.macpry.kotlinnotes.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/* Copyright © Maciej Przybyl * All rights reserved. */

class NoteUI : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {

        linearLayout {
            lparams {
                width = matchParent
                height = wrapContent
            }

            cardView {
                lparams {
                    width = matchParent
                    height = wrapContent
                    useCompatPadding = true
                }
                linearLayout {
                    lparams {
                        orientation = LinearLayout.VERTICAL
                    }

                    textView {
                        lparams {
                            textSize = 20F
                            margin = 5
                        }
                        id = R.id.notes_note_name
                    }

                    textView {
                        lparams {
                            textSize = 15F
                            margin = 5
                        }
                        id = R.id.notes_note_description
                    }

                    textView {
                        lparams {
                            textSize = 15F
                            margin = 5
                        }
                        id = R.id.notes_note_date
                    }
                }
            }
        }
    }
}