package com.macpry.kotlinnotes.notes.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.macpry.kotlinnotes.R
import com.macpry.kotlinnotes.data.Note
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick

/* Copyright © Maciej Przybyl * All rights reserved. */

class NotesAdapter(var notesList: MutableList<Note>, val notesItemListener: NotesItemListener)
    : RecyclerView.Adapter<NotesAdapter.NoteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NoteViewHolder? =
            NoteViewHolder(NoteUI().createView(AnkoContext.create(parent!!.context, parent)))

    override fun getItemCount() = notesList.size

    override fun onBindViewHolder(holder: NoteViewHolder?, position: Int) {
        val note = notesList[position]
        holder!!.name.text = note.name
        holder.description.text = note.description
        holder.date.text = note.date.toString()
        holder.onClick(notesItemListener)
    }

    class NoteViewHolder(noteView: View) : RecyclerView.ViewHolder(noteView) {
        val name: TextView = noteView.find(R.id.notes_note_name)
        val description: TextView = noteView.find(R.id.notes_note_description)
        val date: TextView = noteView.find(R.id.notes_note_date)
    }

    //todo CONSIDER EXTENSION FUNCTIONS
    fun removeNote(note: Note?, adapterPosition: Int) {
        notesList.remove(note)
        notifyItemRemoved(adapterPosition)
        notifyItemRangeRemoved(adapterPosition, 0)
    }

    fun refresh(notesList: MutableList<Note>) {
        //todo CONSIDER PASSING EMPTY LIST CASE
        if (checkNotNull(notesList).isNotEmpty()) {
            this.notesList.clear()
            this.notesList.addAll(notesList)
            notifyDataSetChanged()
        }
    }

    fun <T : RecyclerView.ViewHolder> T.onClick(notesItemListener: NotesItemListener): T {
        itemView.onClick { }
        itemView.setOnClickListener {
            notesItemListener.onNoteShortClick.invoke(notesList[adapterPosition], adapterPosition)
        }
        itemView.setOnLongClickListener {
            notesItemListener.onNoteLongClick.invoke(notesList[adapterPosition], adapterPosition)
        }
        return this
    }

    interface NotesItemListener {
        //todo ENSURE THAT'S GOOD IDEA - CHECK PERFORMANCE AND OBJECT CREATIONS
        val onNoteShortClick: (Note, Int) -> Unit
        val onNoteLongClick: (Note, Int) -> Boolean
    }
}