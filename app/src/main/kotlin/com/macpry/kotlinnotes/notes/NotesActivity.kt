package com.macpry.kotlinnotes.notes

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.macpry.kotlinnotes.App
import com.macpry.kotlinnotes.R
import com.macpry.kotlinnotes.notes.ui.NotesActivityUI
import com.macpry.kotlinnotes.utils.RealmController
import com.macpry.kotlinnotes.utils.addFragmentToActivity
import io.realm.Realm
import org.jetbrains.anko.setContentView
import javax.inject.Inject

/* Copyright © Maciej Przybyl * All rights reserved. */

class NotesActivity : AppCompatActivity() {

    @Inject lateinit var notesPresenter: NotesPresenter

    val notesFragmentId = R.id.notes_fragment_container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        NotesActivityUI().setContentView(this)

        val notesFragment: NotesFragment
        if (supportFragmentManager.findFragmentById(notesFragmentId) == null) {
            notesFragment = NotesFragment.newInstance()
            addFragmentToActivity(notesFragmentId, notesFragment)
        } else {
            notesFragment = supportFragmentManager.findFragmentById(notesFragmentId) as NotesFragment
        }

        DaggerNotesPresenterComponent.builder()
                .appComponent(App.appComponent)
                .notesPresenterModule(NotesPresenterModule(notesFragment))
                .build()
                .inject(this)
    }
}
