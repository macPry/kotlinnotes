package com.macpry.kotlinnotes.notes

import com.macpry.kotlinnotes.data.Note
import com.macpry.kotlinnotes.data.NotesRepository
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

/* Copyright © Maciej Przybyl * All rights reserved. */

class NotesPresenter @Inject constructor(
        private val notesRepository: NotesRepository, private val notesView: NotesContract.View)
    : NotesContract.Presenter {

    val compositeSubscription = CompositeSubscription()

    @Inject fun setupListeners() {
        notesView.setPresenter(this)
    }

    override fun subscribe() {
        loadNotes()
    }

    override fun unsubscribe() {
        compositeSubscription.clear()
    }

    override fun loadNotes() {
        notesView.setLoadingNotesIndicator(true)
        compositeSubscription.add(notesRepository.getNotes().subscribe(
                {
                    notesView.showNotes(it)
                },
                {
                    it.printStackTrace()
                },
                {
                    notesView.setLoadingNotesIndicator(false)
                }
        ))
    }

    override fun openNoteDetails(note: Note?) {
        notesView.showNoteDetailsUI(note?.id)
    }

    override fun openAddNote() {
        notesView.showAddNoteUI()
    }

    override fun deleteNote(note: Note?, adapterPosition: Int) {
        //todo MAYBE STH ELSE - FOR BETTER SYNCHRO WITH REMOTE
        //todo REMOVE VIEW AFTER CALLBACK FROM REPO
        notesRepository.deleteNote(note?.id.toString())
        notesView.removeNoteView(note, adapterPosition)
    }
}