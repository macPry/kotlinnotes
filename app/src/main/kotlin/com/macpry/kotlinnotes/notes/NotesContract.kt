package com.macpry.kotlinnotes.notes

import com.macpry.kotlinnotes.BasePresenter
import com.macpry.kotlinnotes.BaseView
import com.macpry.kotlinnotes.data.Note

/* Copyright © Maciej Przybyl * All rights reserved. */

interface NotesContract {

    interface View : BaseView<Presenter> {
        fun setLoadingNotesIndicator(isLoading: Boolean)
        fun showNotes(notesList: MutableList<Note>)
        fun showLoadingNotesError()
        fun showNoteDetailsUI(noteId: String?)
        fun showAddNoteUI()
        fun removeNoteView(note: Note?, adapterPosition: Int)
    }

    interface Presenter : BasePresenter {
        fun loadNotes()
        fun openNoteDetails(note: Note?)
        fun openAddNote()
        fun deleteNote(note: Note?, adapterPosition: Int)
    }
}