package com.macpry.kotlinnotes.notes.ui

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.macpry.kotlinnotes.notes.NotesFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/* Copyright © Maciej Przybyl * All rights reserved. */

class NotesFragmentUI(val notesAdapter: NotesAdapter) : AnkoComponent<NotesFragment> {

    override fun createView(ui: AnkoContext<NotesFragment>): View = with(ui) {

        relativeLayout {

            recyclerView {
                lparams {
                    width = matchParent
                    height = matchParent
                }
                adapter = notesAdapter
                layoutManager = LinearLayoutManager(ctx)
            }
        }
    }
}