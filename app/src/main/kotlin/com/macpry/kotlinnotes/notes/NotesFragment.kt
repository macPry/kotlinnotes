package com.macpry.kotlinnotes.notes

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.macpry.kotlinnotes.R
import com.macpry.kotlinnotes.data.Note
import com.macpry.kotlinnotes.notes.ui.NotesAdapter
import com.macpry.kotlinnotes.notes.ui.NotesFragmentUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.annotations.NotNull

/* Copyright © Maciej Przybyl * All rights reserved. */

class NotesFragment : Fragment(), NotesContract.View, NotesAdapter.NotesItemListener {

    private var notesPresenter: NotesContract.Presenter? = null

    private val notesAdapter = NotesAdapter(mutableListOf<Note>(), this)

    companion object {
        fun newInstance() = NotesFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notesPresenter?.openRealm()
    }

    override fun onResume() {
        super.onResume()
        notesPresenter?.subscribe()
    }

    override fun onPause() {
        super.onPause()
        notesPresenter?.unsubscribe()
    }

    override fun onDestroy() {
        super.onDestroy()
        notesPresenter?.closeRealm()
    }

    override fun onCreateView(
            inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val floatingButton: FloatingActionButton = activity.find(R.id.notes_fab)
        floatingButton.setOnClickListener { notesPresenter?.openAddNote() }

        return NotesFragmentUI(notesAdapter).createView(AnkoContext.create(context, this))
    }

    override fun setPresenter(@NotNull presenter: NotesContract.Presenter) {
        notesPresenter = checkNotNull(presenter)
    }

    override fun setLoadingNotesIndicator(isLoading: Boolean) {
        toast("Notes Indicator $isLoading")
    }

    override fun showNotes(notesList: MutableList<Note>) {
        notesAdapter.refresh(notesList)
    }

    override fun showLoadingNotesError() {
        toast("Notes loading error")
    }

    override val onNoteShortClick: (Note, Int) -> Unit =
            { note: Note, adapterPosition: Int -> notesPresenter?.openNoteDetails(note) }

    override val onNoteLongClick: (Note, Int) -> Boolean =
            { note: Note, adapterPosition: Int -> notesPresenter?.deleteNote(note, adapterPosition); true }

    override fun showNoteDetailsUI(noteId: String?) {
        toast("showNoteDetailsUI $noteId")
    }

    override fun showAddNoteUI() {
        toast("showAddNoteUI")
    }

    override fun removeNoteView(note: Note?, adapterPosition: Int) {
        notesAdapter.removeNote(note, adapterPosition)
    }
}