package com.macpry.kotlinnotes.notes

import dagger.Module
import dagger.Provides

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class NotesPresenterModule(private val notesView: NotesContract.View) {

    @Provides
    fun provideNotesContractView() = notesView
}