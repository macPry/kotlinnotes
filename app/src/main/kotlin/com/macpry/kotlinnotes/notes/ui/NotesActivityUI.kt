package com.macpry.kotlinnotes.notes.ui

import android.view.Gravity
import com.macpry.kotlinnotes.R
import com.macpry.kotlinnotes.notes.NotesActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.floatingActionButton

/* Copyright © Maciej Przybyl * All rights reserved. */

class NotesActivityUI : AnkoComponent<NotesActivity> {

    override fun createView(ui: AnkoContext<NotesActivity>) = with(ui) {

        coordinatorLayout {
            lparams {
                width = matchParent
                height = matchParent
            }

            frameLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                }
                id = R.id.notes_fragment_container
            }

            floatingActionButton {
                lparams {
                    margin = dip(20)
                    gravity = Gravity.BOTTOM or Gravity.END
                }
                id = R.id.notes_fab
            }
        }
    }
}