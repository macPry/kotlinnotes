package com.macpry.kotlinnotes.notes

import com.macpry.kotlinnotes.di.ActivityScope
import com.macpry.kotlinnotes.di.AppComponent
import dagger.Component

/* Copyright © Maciej Przybyl * All rights reserved. */

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(NotesPresenterModule::class))
interface NotesPresenterComponent {
    fun inject(notesActivity: NotesActivity)
}