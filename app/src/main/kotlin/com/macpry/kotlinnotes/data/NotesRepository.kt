package com.macpry.kotlinnotes.data

import com.macpry.kotlinnotes.data.di.Local
import rx.Observable
import javax.inject.Inject
import javax.inject.Singleton

/* Copyright © Maciej Przybyl * All rights reserved. */

@Singleton
class NotesRepository @Inject constructor(
        @Local private val localDataSource: NotesDataSource) : NotesDataSource {

    override fun getNotes(): Observable<MutableList<Note>> {
        return localDataSource.getNotes()
    }

    override fun getNote(noteId: String?): Observable<Note> {
        return localDataSource.getNote(noteId)
    }

    override fun saveNote(note: Note?) {
        localDataSource.saveNote(note)
    }

    override fun deleteNote(noteId: String?) {
        localDataSource.deleteNote(noteId)
    }
}