package com.macpry.kotlinnotes.data.di

import javax.inject.Qualifier

/* Copyright © Maciej Przybyl * All rights reserved. */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class Local