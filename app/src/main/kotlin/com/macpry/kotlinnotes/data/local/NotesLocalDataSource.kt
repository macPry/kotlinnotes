package com.macpry.kotlinnotes.data.local

import com.macpry.kotlinnotes.data.Note
import com.macpry.kotlinnotes.data.NotesDataSource
import com.macpry.kotlinnotes.utils.RealmController
import rx.Observable
import javax.inject.Singleton

/* Copyright © Maciej Przybyl * All rights reserved. */

@Singleton
class NotesLocalDataSource : NotesDataSource {

    /*
    * DON'T CALL .subscribeOn() ON REALM OBSERVABLES!!!
    * USE REALM'S API .findAllAsync() INSTEAD
    */

    override fun getNotes() = getNotesWithRealm()

    override fun getNote(noteId: String?) = getNoteWithRealm(noteId)

    override fun saveNote(note: Note?) {
        RealmController.realm!!.executeTransactionAsync {
            it.copyToRealmOrUpdate(note)
        }
    }

    override fun deleteNote(noteId: String?) {
        RealmController.realm!!.executeTransactionAsync { it ->
            val result = it.where(Note::class.java)
                    .equalTo(Note::id.name, noteId)
                    .findFirst()
            result.deleteFromRealm()
        }
    }

    private fun getNotesWithRealm(): Observable<MutableList<Note>> {
        return RealmController.realm!!.where(Note::class.java)
                .findAllSortedAsync(Note::date.name)
                .asObservable()
                .filter { it.isLoaded }
                .first()
                .map { it }
    }

    private fun getNoteWithRealm(noteId: String?): Observable<Note> {
        return RealmController.realm!!.where(Note::class.java)
                .equalTo(Note::id.name, noteId)
                .findFirstAsync()
                .asObservable()
    }
}