package com.macpry.kotlinnotes.data

import rx.Observable

/* Copyright © Maciej Przybyl * All rights reserved. */

//todo PRECISE NULL CHECKS - ANNOTATION OR BY KOTLIN "?"

interface NotesDataSource {

    fun getNotes(): Observable<MutableList<Note>>

    fun getNote(noteId: String?): Observable<Note>

    //todo CONSIDER CHANGING save and delete to observable - for callback after proceeded
    fun saveNote(note: Note?)

    fun deleteNote(noteId: String?)
}