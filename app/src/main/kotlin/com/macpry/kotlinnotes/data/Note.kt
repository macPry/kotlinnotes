package com.macpry.kotlinnotes.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

/* Copyright © Maciej Przybyl * All rights reserved. */

@RealmClass
open class Note(

        @PrimaryKey
        @Expose
        @SerializedName("id")
        open var id: String? = UUID.randomUUID().toString(),

        @Expose
        @SerializedName("name")
        open var name: String? = null,

        @Expose
        @SerializedName("date")
        open var date: Date? = Date(),

        @Expose
        @SerializedName("description")
        open var description: String? = null
) : RealmObject()
