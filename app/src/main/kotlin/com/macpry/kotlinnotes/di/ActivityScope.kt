package com.macpry.kotlinnotes.di

import javax.inject.Scope

/* Copyright © Maciej Przybyl * All rights reserved. */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope