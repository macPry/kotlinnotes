package com.macpry.kotlinnotes.di

import android.app.Application
import dagger.Module
import dagger.Provides

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class AppModule(private val application: Application) {

    @Provides
    fun provideContext() = application.applicationContext
}