package com.macpry.kotlinnotes.di

import android.content.Context
import com.macpry.kotlinnotes.data.NotesRepository
import com.macpry.kotlinnotes.data.di.NotesRepositoryModule
import dagger.Component
import javax.inject.Singleton

/* Copyright © Maciej Przybyl * All rights reserved. */

@Singleton
@Component(modules = arrayOf(AppModule::class, NotesRepositoryModule::class))
interface AppComponent {
    fun getContext(): Context
    fun getNotesRepository(): NotesRepository
}