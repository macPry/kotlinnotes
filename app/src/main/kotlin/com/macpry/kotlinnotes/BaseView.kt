package com.macpry.kotlinnotes

import org.jetbrains.annotations.NotNull

/* Copyright © Maciej Przybyl * All rights reserved. */

interface BaseView<in T> {
    fun setPresenter(@NotNull presenter: T)
}