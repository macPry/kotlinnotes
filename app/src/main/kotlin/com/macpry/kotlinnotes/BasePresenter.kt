package com.macpry.kotlinnotes

import com.macpry.kotlinnotes.utils.RealmController

/* Copyright © Maciej Przybyl * All rights reserved. */

interface BasePresenter {
    fun subscribe()
    fun unsubscribe()
    fun openRealm() = RealmController.open()
    fun closeRealm() = RealmController.close()
}