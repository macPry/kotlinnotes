package com.macpry.kotlinnotes.utils

import io.realm.Realm

/* Copyright © Maciej Przybyl * All rights reserved. */

object RealmController {

    var realm: Realm? = null
        private set

    @Synchronized fun open() {
        realm = Realm.getDefaultInstance()
    }

    @Synchronized fun close() {
        realm?.close()
    }
}