@file:JvmName("ActivityUtils")

package com.macpry.kotlinnotes.utils

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import org.jetbrains.annotations.NotNull

/* Copyright © Maciej Przybyl * All rights reserved. */

fun AppCompatActivity.addFragmentToActivity(fragmentId: Int, @NotNull fragment: Fragment) {
    checkNotNull(fragment)
    checkNotNull(supportFragmentManager)
    val fragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.add(fragmentId, fragment)
    fragmentTransaction.commit()
}