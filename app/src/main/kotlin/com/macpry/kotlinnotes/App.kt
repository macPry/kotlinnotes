package com.macpry.kotlinnotes

import android.app.Application
import com.macpry.kotlinnotes.data.di.NotesRepositoryModule
import com.macpry.kotlinnotes.di.AppComponent
import com.macpry.kotlinnotes.di.AppModule
import com.macpry.kotlinnotes.di.DaggerAppComponent
import io.realm.Realm

/* Copyright © Maciej Przybyl * All rights reserved. */

class App : Application() {

    companion object {
        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .notesRepositoryModule(NotesRepositoryModule())
                .build()
    }
}