package com.macpry.kotlinnotes.data.di

import com.macpry.kotlinnotes.data.NotesDataSource
import com.macpry.kotlinnotes.data.local.NotesLocalDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/* Copyright © Maciej Przybyl * All rights reserved. */

@Module
class NotesRepositoryModule {

    @Provides
    @Singleton
    @Local
    fun provideNotesLocalDataSource(): NotesDataSource = NotesLocalDataSource()
}