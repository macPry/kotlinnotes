package com.macpry.kotlinnotes.data.local

import com.macpry.kotlinnotes.data.Note
import com.macpry.kotlinnotes.data.NotesDataSource
import rx.Observable

/* Copyright © Maciej Przybyl * All rights reserved. */

class FakeNotesLocalDataSource : NotesDataSource {

    companion object {
        val notesList = mutableListOf<Note>()

        init {
            (1..50).mapTo(notesList) { Note(id = "id_$it", name = "Name $it", description = "Desc $it") }
        }
    }

    override fun getNotes(): Observable<MutableList<Note>> = Observable.just(notesList)

    override fun getNote(noteId: String?): Observable<Note> = notesList
            .firstOrNull { it.id == noteId }
            ?.let { Observable.just(it) }
            ?: Observable.error(Exception("Note not found"))

    override fun saveNote(note: Note?) {
        if (note != null) {
            notesList.add(note)
        }
    }

    override fun deleteNote(noteId: String?) {
        for (i in notesList) {
            if (i.id == noteId) {
                notesList.remove(i)
                break
            }
        }
    }
}